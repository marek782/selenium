import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import sun.rmi.runtime.Log;

import java.util.concurrent.TimeUnit;

/**
 * Created by mark on 23.05.18.
 */
public class LoginTest {

    final String FACEBOOK_LOGIN_URL = "https://en-gb.facebook.com/login/";

    @Test
    public void facebookLogin() {

        WebDriver driver = new FirefoxDriver();

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        driver.navigate().to("https://en-gb.facebook.com/login/");

        driver.findElement(By.id("email")).sendKeys("dsa");
        driver.findElement(By.id("pass")).sendKeys("Fds");
        driver.findElement(By.id("loginbutton")).submit();

        Assert.assertEquals(false, driver.getPageSource().contains("Log Out"));

    }
}
